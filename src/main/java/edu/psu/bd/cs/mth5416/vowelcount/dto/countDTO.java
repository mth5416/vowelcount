package edu.psu.bd.cs.mth5416.vowelcount.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class countDTO
{
    private int aCount, eCount, iCount, oCount, uCount;
}