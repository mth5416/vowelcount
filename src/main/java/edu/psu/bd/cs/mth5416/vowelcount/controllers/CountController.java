package edu.psu.bd.cs.mth5416.vowelcount.controllers;

import edu.psu.bd.cs.mth5416.vowelcount.dto.countDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountController
{
    @GetMapping(path = "/count")
    public countDTO countVowels(@RequestParam(name = "word") String word)
    {
        countDTO count = new countDTO(0,0,0,0,0);
        word = word.toLowerCase();
        for (int i = 0; i < word.length(); i++)
        {
            char c = word.charAt(i);

            switch(c)
            {
                case 'a':
                    count.setACount(count.getACount() + 1);
                    break;
                case 'e':
                    count.setECount(count.getECount() + 1);
                    break;
                case 'i':
                    count.setICount(count.getICount() + 1);
                    break;
                case 'o':
                    count.setOCount(count.getOCount() + 1);
                    break;
                case 'u':
                    count.setUCount(count.getUCount() + 1);
                    break;
            }
        }

        return count;
    }
}
