package edu.psu.bd.cs.mth5416.vowelcount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VowelcountApplication {

    public static void main(String[] args) {
        SpringApplication.run(VowelcountApplication.class, args);
    }

}
